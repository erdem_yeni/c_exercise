OSFLAG :=
RM :=
ifeq ($(OS),Windows_NT)
	RM := del
	OSFLAG += -D WIN32
	ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
		OSFLAG += -D AMD64
	endif
	ifeq ($(PROCESSOR_ARCHITECTURE),x86)
		OSFLAG += -D IA32
	endif
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		RM := rm
		OSFLAG += -D LINUX
	endif
	ifeq ($(UNAME_S),Darwin)
		OSFLAG += -D OSX
	endif
		UNAME_P := $(shell uname -p)
	ifeq ($(UNAME_P),x86_64)
		OSFLAG += -D AMD64
	endif
		ifneq ($(filter %86,$(UNAME_P)),)
	OSFLAG += -D IA32
		endif
	ifneq ($(filter arm%,$(UNAME_P)),)
		OSFLAG += -D ARM
	endif
endif

SRC_DIR := Source
BLD_DIR := Build
OBJ_DIR := Object

OBJ_FILE := $(OBJ_DIR)\\*.o
BLD_FILE := $(BLD_DIR)\\*.exe

all: main
	echo $(OSFLAG)
	echo "Main building."

main: main.o
	g++ -Wall $(OBJ_DIR)\\main.o -o $(BLD_DIR)\\main.exe

main.o: $(SRC_DIR)\\main.cpp
	g++ -Wall -c $(SRC_DIR)\\main.cpp -o $(OBJ_DIR)\\main.o

clean:
	$(RM) $(OBJ_FILE) $(BLD_FILE)